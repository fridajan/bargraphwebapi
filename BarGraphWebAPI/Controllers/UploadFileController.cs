﻿using BarGraphWebAPI.Helpers;
using BarGraphWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BarGraphWebAPI.Controllers
{
    public class UploadFileController : ApiController
    {
        // POST: api/UploadFile/UploadFile
        public async Task<HttpResponseMessage> Post()
        {
            try
            {
                List<Bar> parsedFile = new List<Bar>();

                if (!Request.Content.IsMimeMultipartContent())
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

                MultipartMemoryStreamProvider provider = new MultipartMemoryStreamProvider();
                await Request.Content.ReadAsMultipartAsync(provider);
                foreach (var file in provider.Contents)
                {
                    string fileContent = await file.ReadAsStringAsync();

                    parsedFile = FileParser.ParseFileContent(fileContent);
                }

                return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(parsedFile));
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
