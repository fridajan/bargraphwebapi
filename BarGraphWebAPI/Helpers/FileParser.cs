﻿using BarGraphWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BarGraphWebAPI.Helpers
{
    public static class FileParser
    {
        public static List<Bar> ParseFileContent(string lines)
        {
            List<Bar> barGraph = new List<Bar>();

            Regex lineRegex = new Regex(@"#([A-Za-z0-9\-]+):([A-Za-z\-]+):([0-9\-]+)");

            var lineList = lines.Split('\n');

            foreach (var line in lineList)
            {
                Match lineMatch = lineRegex.Match(line);

                if (lineMatch.Success)
                {
                    Bar bar = new Bar
                    {
                        Name = lineMatch.Groups[1].Value,
                        Color = lineMatch.Groups[2].Value,
                        Size = Convert.ToInt32(lineMatch.Groups[3].Value)
                    };

                    barGraph.Add(bar);
                }
                else
                {
                    throw new Exception("Error processing file. File is not formatted correctly. Format should be on the form #NAME:COLOR:SIZE");
                }
            }

            return barGraph;
        }
    }
}