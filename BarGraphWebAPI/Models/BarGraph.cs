﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BarGraphWebAPI.Models
{
    public class Bar
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int Size { get; set; }
    }
}