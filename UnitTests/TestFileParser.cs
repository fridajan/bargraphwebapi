﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BarGraphWebAPI.Helpers;

namespace UnitTests
{
    [TestClass]
    public class TestFileParser
    {
        [TestMethod]
        public void ParseFileContent_ShouldReturnListOfBars()
        {
            string testdata = "#A:RED:5\n#B:BLUE:10";

            var result = FileParser.ParseFileContent(testdata);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("A", result[0].Name);
            Assert.AreEqual("RED", result[0].Color);
            Assert.AreEqual(5, result[0].Size);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ParseFileContent_ShouldReturnError()
        {
            string testdata = "A:RED:5";

            var result = FileParser.ParseFileContent(testdata);
        }

    }
}
